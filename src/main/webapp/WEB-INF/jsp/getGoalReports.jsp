<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Goal Reports</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
</head>
<body>
  <div class="container">
    <table class="table table-bordered">
      <tr>
        <th>Goal ID</th>
        <th>Goal Minutes</th>
        <th>Exercise Minutes</th>
        <th>Exercise Activity</th>
      </tr>
      <c:forEach items="${goalReports}" var="goalReport">
        <tr>
          <td>${goalReport.goalId}</td>
          <td>${goalReport.goalMinutes}</td>
          <td>${goalReport.exerciseMinutes}</td>
          <td>${goalReport.exerciseActivity}</td>
        </tr>
      </c:forEach>
    </table>
  </div>
</body>
</html>
