<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Goals Report</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
</head>
<body>
<h1>All goals</h1>
<table class="table table-hover" >
  <tr>
    <th>Goal ID</th><th>Minutes</th>
  </tr>
  <c:forEach items="${goals}" var="goal">
    <tr>
      <td>${goal.id}</td>
      <td>${goal.minutes}</td>
      <td>
        <table class="table table-hover" >
          <tr>
            <th>exercise id</th> <th> minutes </th> <th> activity </th>
          </tr>
          <c:forEach items="${goal.exercises}" var="exercise">
            <tr>
              <td>${exercise.id}</td>
              <td>${exercise.minutes}</td>
              <td>${exercise.activity}</td>
            </tr>
          </c:forEach>
        </table>
      </td>
    </tr>
  </c:forEach>
</table>
</body>
</html>
