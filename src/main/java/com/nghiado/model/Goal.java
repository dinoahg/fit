package com.nghiado.model;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.util.List;


@Entity
public class Goal {

	@Id
	@GeneratedValue
	private Long id;

	@Range(min = 1, max = 120)
	private int minutes;

	@OneToMany(mappedBy = "goal", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Exercise> exercises;


	public List<Exercise> getExercises() {
		return exercises;
	}

	public void setExercises(List<Exercise> exercises) {
		this.exercises = exercises;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

}
