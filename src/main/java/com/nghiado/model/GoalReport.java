package com.nghiado.model;

/**
 * Created by nghiado on 2/12/2016.
 */
public class GoalReport {
    private Long goalId;
    private int goalMinutes;
    private int exerciseMinutes;
    private String exerciseActivity;

    public GoalReport(Long goalId, int goalMinutes, int exerciseMinutes, String exerciseActivity) {
        this.goalId = goalId;
        this.goalMinutes = goalMinutes;
        this.exerciseMinutes = exerciseMinutes;
        this.exerciseActivity = exerciseActivity;
    }

    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }

    public int getGoalMinutes() {
        return goalMinutes;
    }

    public void setGoalMinutes(int goalMinutes) {
        this.goalMinutes = goalMinutes;
    }

    public int getExerciseMinutes() {
        return exerciseMinutes;
    }

    public void setExerciseMinutes(int exerciseMinutes) {
        this.exerciseMinutes = exerciseMinutes;
    }

    public String getExerciseActivity() {
        return exerciseActivity;
    }

    public void setExerciseActivity(String exerciseActivity) {
        this.exerciseActivity = exerciseActivity;
    }
}
