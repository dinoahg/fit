package com.nghiado.repository;

import com.nghiado.model.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by nghiado on 2/3/2016.
 */
@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, Long> {

}
