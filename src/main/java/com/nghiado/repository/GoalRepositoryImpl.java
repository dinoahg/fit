package com.nghiado.repository;

import com.nghiado.model.Goal;
import com.nghiado.model.GoalReport;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by nghiado on 2/2/2016.
 */
//@Repository
public class GoalRepositoryImpl/* implements GoalRepository */{

    @PersistenceContext
    private EntityManager em;

    public Goal save(Goal goal) {

        if (goal.getId() == null) {
            em.persist(goal);
            em.flush();
        }
        else {
            goal = em.merge(goal);
        }
        return goal;
    }

    public List<Goal> loadAll() {
        Query query = em.createQuery("SELECT g from Goal g");
        return query.getResultList();
    }

    public List<GoalReport> findGoalReports() {
        Query query = em.createQuery("select new com.nghiado.model.GoalReport(g.id, g.minutes, e.minutes, e.activity) " +
                "from Goal g, Exercise e where g.id = e.goal.id");
        return query.getResultList();
    }
}
