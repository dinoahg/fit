package com.nghiado.repository;

import com.nghiado.model.Exercise;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by nghiado on 2/3/2016.
 */
//@Repository
public class ExerciseRepositoryImpl /*implements ExerciseRepository*/ {

    @PersistenceContext
    private EntityManager entityManager;

    public Exercise save(Exercise exercise) {

        entityManager.persist(exercise);
        entityManager.flush();

        return exercise;
    }
}
