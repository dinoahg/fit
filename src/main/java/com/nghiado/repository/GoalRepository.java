package com.nghiado.repository;

import com.nghiado.model.Goal;
import com.nghiado.model.GoalReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by nghiado on 2/2/2016.
 */
@Repository
public interface GoalRepository extends JpaRepository<Goal, Long>{

    @Query("select new com.nghiado.model.GoalReport(g.id, g.minutes, e.minutes, e.activity) " +
            "from Goal g, Exercise e where g.id = e.goal.id")
    List<GoalReport> findGoalReports();
}
