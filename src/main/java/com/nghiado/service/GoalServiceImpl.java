package com.nghiado.service;

import com.nghiado.model.Goal;
import com.nghiado.model.GoalReport;
import com.nghiado.repository.GoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by nghiado on 2/2/2016.
 */
@Service
public class GoalServiceImpl implements GoalService{

    @Autowired
    private GoalRepository goalRepository;

    @Transactional
    public Goal save(Goal goal) {
        return goalRepository.save(goal);

    }

    @Transactional
    public List<Goal> loadAll() {
        return goalRepository.findAll();
    }

    public List<GoalReport> findGoalReports() {
        return goalRepository.findGoalReports();
    }
}