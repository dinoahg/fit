package com.nghiado.service;

import com.nghiado.model.Goal;
import com.nghiado.model.GoalReport;

import java.util.List;

/**
 * Created by nghiado on 2/2/2016.
 */
public interface GoalService {
    Goal save(Goal goal);

    List<Goal> loadAll();

    List<GoalReport> findGoalReports();
}
