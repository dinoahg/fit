package com.nghiado.service;

import java.util.List;

import com.nghiado.model.Activity;
import com.nghiado.model.Exercise;

public interface ExerciseService {

	List<Activity> findAllActivities();

	Exercise save(Exercise exercise);
}